#!/bin/bash

export DISPLAY=:1
[[ -z "$USER" ]] && USER=USER
[[ -z "$SESSION_MANAGER" ]] && SESSION_MANAGER=xfce4-session

./enc_mount.sh &&
  sudo mkdir -p /mnt/root &&
  sudo ln -sf /mnt/encrypted /mnt/base/home &&
  ./init.sh &&
  sudo mount -t aufs -o br=/mnt/base:/mnt/live none /mnt/root &&
  sudo mount -o bind /proc /mnt/root/proc/ &&
  sudo mount -o bind /sys /mnt/root/sys/ &&
  sudo mount -o bind /dev /mnt/root/dev/ &&
  sudo mount -o bind /dev/pts /mnt/root/dev/pts/ &&
  (cd / &&
  sudo chroot /mnt/root su root /bin/bash -c "export DISPLAY=$DISPLAY
service dbus start")

