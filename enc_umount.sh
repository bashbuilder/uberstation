#!/bin/bash

sleep 0.3

sudo umount /mnt/root/dev/pts
sudo umount /mnt/root/dev
sudo umount /mnt/root/sys
sudo umount /mnt/root/proc
sudo umount /dev/mapper/encrypted
sudo umount /mnt/base
sudo umount /mnt/root
sudo cryptsetup luksClose /dev/mapper/encrypted

