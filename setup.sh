#!/bin/bash

sudo cryptsetup luksFormat /dev/sdc2
sudo cryptsetup luksOpen /dev/sdc2 encrypted
sudo mkfs.ext4 /dev/mapper/encrypted

