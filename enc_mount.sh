#!/bin/bash

sudo mkdir -p /mnt/live &&
  sudo mount -t squashfs /cdrom/casper/filesystem.squashfs /mnt/live

sudo mkdir -p /mnt/base &&
  sudo cryptsetup luksOpen /dev/sdc2 encrypted &&
  sudo mount /dev/sdc1 /mnt/base &&
  sudo mkdir -p /mnt/base/mnt/encrypted &&
  sudo mount /dev/mapper/encrypted /mnt/base/mnt/encrypted

